package com.codetalenta.katalogbuku;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    SimpleAdapter simpleAdapter;
    HashMap<String, String> hashMap;
    ArrayList<HashMap<String, String>> mylist;
    String[] cover;
    String[] judul;
    String[] genre;
    String[] rating;
    String[] harga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.list_view);
        judul = new String[]{"Final Fantasy1", "Warkop DKI", "Final Fantasy3", "Final Fantasy4"};
        genre = new String[]{"Action", "Comedy", "Romance", "Action"};
        rating = new String[]{"4.0", "5,0", "2.0", "3.0"};
        harga = new String[]{"IDR 32,000", "IDR 20,000", "IDR 40,000", "IDR 10,000"};
        cover = new String[]{Integer.toString(R.drawable.cover), Integer.toString(R.drawable.cover), Integer.toString(R.drawable.wakop), Integer.toString(R.drawable.cover)
        };

        mylist = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < judul.length; i++) {
            hashMap = new HashMap<String, String>();
            hashMap.put("judul", judul[i]);
            hashMap.put("genre", genre[i]);
            hashMap.put("rating", rating[i]);
            hashMap.put("harga", harga[i]);
            hashMap.put("cover", cover[i]);
            mylist.add(hashMap);
        }

        simpleAdapter = new SimpleAdapter(MainActivity.this, mylist, R.layout.list_film,
                new String[]{"judul", "genre", "rating", "harga", "cover"},
                new int[]{R.id.judul, R.id.kategori, R.id.rating, R.id.harga, R.id.cover});
        listView.setAdapter(simpleAdapter);
    }
}